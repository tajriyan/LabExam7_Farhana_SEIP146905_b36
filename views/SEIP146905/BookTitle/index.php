<head>
    <link rel="stylesheet" href="../../../resource/asset/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/asset/font-awesome/css/font-awesome.min.css">
    <script src="../../../resource/asset/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/asset/bootstrap/js/bootstrap.min.js"></script>
</head>

<?php

require_once("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;
use App\Message\Message;


$objBookTitle = new BookTitle();

$allData = $objBookTitle->index("object");
$serial = 1;
echo "<table border='5px' >";

echo "<th> Serial </th>";
echo "<th> ID </th>";
echo "<th> Book Title </th>";
echo "<th> Author Name </th>";
echo "<th> Action </th>";


foreach($allData as $oneData){
    echo "<tr style='height: 40px'>";
    echo "<td>".$serial."</td>";

    echo "<td>".$oneData->id."</td>";
    echo "<td>".$oneData->book_title."</td>";
    echo "<td>".$oneData->author_name."</td>";


    echo "<td>";

    echo "<a href='view.php?id=$oneData->id'><button class='btn btn-info'>View</button></a> ";
    echo "<a href='edit.php?id=$oneData->id'><button class='btn btn-primary'>Edit</button></a> ";
    echo "<a href='delete.php?id=$oneData->id'><button class='btn btn-danger'>Delete</button></a> ";


    echo "</td>";

    echo "</tr>";

    $serial++;
}

echo "</table>";
