<?php
namespace App\SummaryOfOrganization;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
class SummaryOfOrganization extends DB{
    public $id;
    public $company_name;
    public $company_summary;
    public function __construct()
    {
        parent:: __construct();
    }
    public function setData($postVariable=null)
    {

        if(array_key_exists("id",$postVariable))
        {
            $this->id =        $postVariable['id'];
        }
        if(array_key_exists("company_name",$postVariable))
        {
            $this->company_name =        $postVariable['company_name'];
        }
        if(array_key_exists("company_summary",$postVariable))
        {
            $this->company_summary =        $postVariable['company_summary'];
        }
    }
    public function store()
    {
        $arrayData=array($this->company_name,$this->company_summary);
        $sql="insert into summary_of_organization(company_name,company_summary)VALUES (?,?)";
        $STH= $this->conn->prepare($sql);
        $result= $STH->execute($arrayData);
        if($result)
            Message::message("data has been inserted successfully");
        else
            Message::message("Failure ....Data is not inserted");
        Utility::redirect('create.php');
    }
}
?>

