<?php
namespace App\ProfilePicture;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
class ProfilePicture extends DB
{
    public $id;
    public $name;
    public $myimage;

    public function __construct()
    {
        parent:: __construct();
    }

    public function setData($postVariable = null)
    {

        if (array_key_exists("id", $postVariable)) {
            $this->id = $postVariable['id'];
        }
        if (array_key_exists("name", $postVariable)) {
            $this->name = $postVariable['name'];
        }
        if (array_key_exists("myimage", $postVariable)) {
            $this->myimage = $postVariable['myimage'];
        }
    }

    public function store()
    {
        $folder = "/xampp/htdocs/Labexam7_Farhana_SEIP146905_b36/image/";
        $path = $folder.time().$_FILES['myimage']['name'];   //this will enter into database
       $img= (move_uploaded_file($_FILES["myimage"]["tmp_name"], $path));
        $arrayData = array($this->name, $path);
        $sql = "insert into profile_picture(name,profile_picture)VALUES (?,?)";
        $STH = $this->conn->prepare($sql);
        $result = $STH->execute($arrayData);
        if($result)
            Message::message("data and image has been inserted successfully");
        else
            Message::message("Failure ....Data is not inserted");

        Utility::redirect('create.php');

    }
}
?>