<?php
namespace App\BirthDay;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
class BirthDay extends DB{
    public $id;
    public $name;
    public $birth_date;
    public function __construct()
    {
        parent:: __construct();
    }
    public function setData($postVariable=null)
    {

        if(array_key_exists("id",$postVariable))
        {
            $this->id =        $postVariable['id'];
        }
        if(array_key_exists("name",$postVariable))
        {
            $this->name =        $postVariable['name'];
        }
        if(array_key_exists("birth_date",$postVariable))
        {
            $this->birth_date =        $postVariable['birth_date'];
        }
    }
    public function store()
    {
        $arrayData=array($this->name,$this->birth_date);
        $sql="insert into birthday(name,birth_date)VALUES (?,?)";
        $STH= $this->conn->prepare($sql);
        $result= $STH->execute($arrayData);
        if($result)
            Message::message("data has been inserted successfully");
        else
            Message::message("Failure ....Data is not inserted");
        Utility::redirect('create.php');
    }
}
?>

