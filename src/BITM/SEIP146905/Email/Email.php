<?php
namespace App\Email;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
class Email extends DB{
    public $id;
    public $name;
    public $email_address;
    public function __construct()
    {
        parent:: __construct();
    }
    public function setData($postVariable=null)
    {

        if(array_key_exists("id",$postVariable))
        {
            $this->id =        $postVariable['id'];
        }
        if(array_key_exists("name",$postVariable))
        {
            $this->name =        $postVariable['name'];
        }
        if(array_key_exists("email_address",$postVariable))
        {
            $this->email_address =        $postVariable['email_address'];
        }
    }
    public function store()
    {
        $arrayData=array($this->name,$this->email_address);
        $sql="insert into email(name,email_address)VALUES (?,?)";
        $STH= $this->conn->prepare($sql);
        $result= $STH->execute($arrayData);
        if($result)
            Message::message("data has been inserted successfully");
        else
            Message::message("Failure ....Data is not inserted");
        Utility::redirect('create.php');
    }
}
?>

