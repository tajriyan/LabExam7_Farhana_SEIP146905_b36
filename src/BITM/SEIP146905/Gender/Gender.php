<?php
namespace App\Gender;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
class Gender extends DB{
    public $id;
    public $name;
    public $gender_name;
    public function __construct()
    {
        parent:: __construct();
    }
    public function setData($postVariable=null)
    {

        if(array_key_exists("id",$postVariable))
        {
            $this->id =        $postVariable['id'];
        }
        if(array_key_exists("name",$postVariable))
        {
            $this->name =        $postVariable['name'];
        }
        if(array_key_exists("gender_name",$postVariable))
        {
            $this->gender_name =        $postVariable['gender_name'];
        }
    }
    public function store(){
        $arrayData=array($this->name,$this->gender_name);
        $sql="insert into gender(name,gender_name)VALUES (?,?)";
        $STH= $this->conn->prepare($sql);
        $result= $STH->execute($arrayData);
        if($result)
            Message::message("data has been inserted successfully");
        else
            Message::message("Failure ....Data is not inserted");

        Utility::redirect('create.php');
    }
}
?>
