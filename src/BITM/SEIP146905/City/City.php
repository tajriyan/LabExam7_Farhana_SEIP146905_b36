<?php
namespace App\City;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
class City extends DB{
    public $id;
    public $name;
    public $city_name;
    public function __construct()
    {
        parent:: __construct();
    }
    public function setData($postVariable=null)
    {

        if(array_key_exists("id",$postVariable))
        {
            $this->id =        $postVariable['id'];
        }
        if(array_key_exists("name",$postVariable))
        {
            $this->name =        $postVariable['name'];
        }
        if(array_key_exists("city_name",$postVariable))
        {
            $this->city_name =        $postVariable['city_name'];
        }
    }
    public function store(){
       $city= " ";
        foreach($this->city_name as $city1)
        {
            $city= $city.$city1;
        }
        $arrayData=array($this->name,$city);
        $sql="insert into city(name,city_name)VALUES (?,?)";
        $STH= $this->conn->prepare($sql);
        $result= $STH->execute($arrayData);
        if($result)
            Message::message("data has been inserted successfully");
        else
            Message::message("Failure ....Data is not inserted");

        Utility::redirect('create.php');
    }
}
?>
