<?php
namespace App\Hobbies;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
class Hobbies extends DB
{
    public $id;
    public $name;
    public $hobby_name;
    public function __construct()
    {
        parent:: __construct();
    }
    public function setData($postVariable=null)
    {

        if(array_key_exists("id",$postVariable))
        {
            $this->id =        $postVariable['id'];
        }
        if(array_key_exists("name",$postVariable))
        {
            $this->name =        $postVariable['name'];
        }
        if(array_key_exists("hobby_name",$postVariable))
        {
            $this->hobby_name =        $postVariable['hobby_name'];
        }
    }
    public function store(){
        $checkbox1=$this->hobby_name;
        $chk="";
        foreach($checkbox1 as $chk1)
        {
            $chk .= $chk1." ";
        }
        $arrayData=array($this->name,$chk);
        $sql="insert into hobbies(name,hobby_name)VALUES (?,?)";
        $STH= $this->conn->prepare($sql);
        $result= $STH->execute($arrayData);
        if($result)
            Message::message("data has been inserted successfully");
        else
            Message::message("Failure ....Data is not inserted");

        Utility::redirect('create.php');
    }
}
?>

